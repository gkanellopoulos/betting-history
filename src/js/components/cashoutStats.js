import React from "react"
import { Line } from "react-chartjs"

export default class CashoutStats extends React.Component {
  render() {
    let labelValue = 0;

    const data = {
        labels: Array.apply(null, new Array(this.props.cashoutWinningsData.length)).
            map((obj, index) => {
              return index;
            }),
        datasets: [
            {
                label: "Potential Payout",
                fillColor: "rgba(0,204,0,0.2)",
                strokeColor: "rgba(0,204,0,1)",
                pointColor: "rgba(0,204,0,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(0,204,0,1)",
                data: Array.apply(null, new Array(10)).map(Number.prototype.valueOf, this.props.potentialPayout)
            },
            {
                label: "Cashout",
                fillColor: "rgba(51,153,205,0.2)",
                strokeColor: "rgba(51,153,205,1)",
                pointColor: "rgba(51,153,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(51,153,205,1)",
                data: this.props.cashoutWinningsData
            },
            {
                label: "Stake",
                fillColor: "rgba(258,154,50,0.2)",
                strokeColor: "rgba(258,154,50,1)",
                pointColor: "rgba(258,154,50,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(258,154,50,1)",
                data: Array.apply(null, new Array(10)).map(Number.prototype.valueOf, this.props.stake*1)
            }
        ]
    };
    const options = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    };
    return <Line data={data} options={options} width="400" height="240"/>
  }
};
