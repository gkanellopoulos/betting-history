import React from "react"
import { connect } from "react-redux"

import { fetchBets } from "../actions/fetchBets"
import CashoutStats from "./cashoutStats"

@connect((store) => {
  return {
    bets: store.bets,
  };
})
export default class BetsHistory extends React.Component {
  fetchBetsReact() {
    this.props.dispatch(fetchBets())
  }

  render() {
    const { bets } = this.props;
    let mappedBets = [];

    if (bets.length) {
      mappedBets = bets.map(bet =>
        <tbody className="table">
          <tr>
            <th>Receipt</th>
            <th>Event</th>
            <th>Selection</th>
            <th>Status</th>
            <th>Stake</th>
            <th>Odds</th>
            <th>Cashout</th>
          </tr>
          <tr key={bet.id} >
              <th>{bet.receipt}</th>
              <th>{bet.name}</th>
              <th>{bet.selection}</th>
              <th>{bet.status}</th>
              <th>£ {bet.stake}</th>
              <th>{bet.odds}</th>
              <th>£ {bet.cashoutWinningsData[bet.cashoutWinningsData.length - 1]}</th>
          </tr>
          <tr>
              <th>
                  <CashoutStats
                      stake={bet.stake}
                      potentialPayout={bet.stake * bet.odds}
                      cashoutWinningsData={bet.cashoutWinningsData}
                  />
              </th>
          </tr>
        </tbody>
      )
    }

    return <div>
      <button onClick={this.fetchBetsReact.bind(this)}>Show bet History</button>

      <table className="table table-hover table-striped">

          {mappedBets}

      </table>
    </div>
  }
}
