import axios from "axios";

export function fetchBets() {
  return function(dispatch) {
    dispatch({type: "FETCH_BETS_START"})
    axios.get("./data/bets.json")
      .then((response) => {
        dispatch({type: "RECEIVE_BETS", response: response['data']})
      })
      .catch((err) => {
        dispatch({type: "FETCH_USERS_ERROR", response: err})
      })
  }
}
