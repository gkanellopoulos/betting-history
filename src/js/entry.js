import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"

import BetsHistory from "./components/betsHistory"
import store from "./store"

store.subscribe(() => {
    console.log("Bet history changeeed", store.getState())
})
console.log();
const app = document.getElementById('app')

ReactDOM.render(<Provider store={store}>
  <BetsHistory />
</Provider>, app);
