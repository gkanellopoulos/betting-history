import { applyMiddleware, combineReducers, createStore } from "redux"
import logger from "redux-logger";
import thunk from "redux-thunk";

import reducer from "./reducers/betsHistoryReducer"

// Runs before action takes place.
const middleware = applyMiddleware(thunk, logger());

export default createStore(reducer, middleware)
