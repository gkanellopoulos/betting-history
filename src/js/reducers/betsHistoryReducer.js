export default function reducer(state={
    fetching: false,
    fetched: false,
    bets: {},
    error: null
  }, action) {

    switch (action.type) {
      case "FETCH_BETS_START": {
        state = {
          ...state,
          fetching: true
        }
        break;
      }
      case "FETCH_BETS_ERROR": {
        state = {
          ...state,
          fetching: false,
          error: action.response
        }
        break;
      }
      case "RECEIVE_BETS": {
        state = {
          ...state,
          fetching: false,
          fetched: true,
          bets: action.response.data
        }
        break;
      }
    }

    return state;
}
